##
# @file This file is part of AWP-ODC-OS.
#
# @author David Lenz (dlenz AT ucsd.edu)
# @author Rajdeep Konwar (rkonwar AT ucsd.edu)
#
# @section LICENSE
# Copyright (c) 2017, Regents of the University of California
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @section DESCRIPTION
# Connection module within the flowTools package. Used to define a connection to manage file transfer and job execution.
##

import os
import saga
import logging
import math


class Connection:
  """ Contains saga context, saga sessions and functions related to submit job """

  def __init__( self, i_root, i_certPath, i_xmlPath ):
    """ Constructor
        Args: i_root      - root of element tree used to parse the xml file
              i_certPath  - path of X509 certificate
              i_xmlPath   - path of the xml file
        Ret:  None
    """

    # Parse basic info from XML
    self.root               = i_root
    self.name               = self.root.find( 'name' ).text
    self.gocdAgent          = self.parseGocdAgent()
    self.gocdPipeline       = self.parseGocdPipeline()
    self.remoteUrl          = self.parseUrl()
    self.remoteBasePath     = self.parseRemoteBasePath()
    self.localPath          = 'localhost/%s' % os.path.dirname( os.path.abspath( i_xmlPath ) )

    # Create SAGA directories for file management
    self.initSagaDirs()

    # Define SAGA security context
    self.context            = saga.Context( "X509" )
    self.context.user_proxy = i_certPath
    self.session            = saga.Session( default = False )
    self.session.add_context( self.context )

    # Initialize SAGA services for job execution
    self.shellService       = saga.job.Service( "gsissh://%s" % self.remoteUrl, session = self.session )
    self.slurmService       = saga.job.Service( "slurm+gsissh://%s" % self.remoteUrl, session = self.session )


  def initSagaDirs( self ):
    """ Creates the following SAGA directory objects to facilitate file transfer:
          localDir          - Working directory on the local machine
          localOutputDir    - Subdirectory of localDir; where all output files will be copied to from remote machine
          localLogDir       - Subdirectory of localOutputDir; where all SAGA log files will be copied to
          remoteBaseDir     - Pre-existing parent directory of the remote working directory
          remoteDir         - Working directory on the remote machine (need not exist beforehand)

        Args: None
        Ret:  None
    """

    self.localDir           = saga.filesystem.Directory( "file://%s" % self.localPath )

    if not self.localDir.is_dir( "%s_output/" % self.name ):
      self.localDir.make_dir( "%s_output/" % self.name )
    self.localOutputDir     = self.localDir.open_dir( "%s_output/" % self.name )

    if not self.localOutputDir.is_dir( "saga_logs/" ):
      self.localOutputDir.make_dir( "saga_logs/" )
    self.localLogDir        = self.localOutputDir.open_dir( "saga_logs/" )

    self.remoteBaseDir      = saga.filesystem.Directory( "gsisftp://%s/%s" % (self.remoteUrl, self.remoteBasePath) )

    # Creates AGENT/pipelines/PIPELINE subdirectory on remote machine in three steps if it doesn't already exist
    if not self.remoteBaseDir.is_dir( "%s/" % self.gocdAgent ):
      self.remoteBaseDir.make_dir( "%s/" % self.gocdAgent )
    l_temp_dir1 = self.remoteBaseDir.open_dir( "%s/" % self.gocdAgent )

    if not l_temp_dir1.is_dir( "pipelines/" ):
      l_temp_dir1.make_dir( "pipelines/" )
    l_temp_dir2 = l_temp_dir1.open_dir( "pipelines/" )

    if not l_temp_dir2.is_dir( "%s/" % self.gocdPipeline ):
      l_temp_dir2.make_dir( "%s/" % self.gocdPipeline )
    self.remoteDir = l_temp_dir2.open_dir( "%s/" % self.gocdPipeline )


  def copyInFiles( self ):
    """ Copies files from local dir to remote dir
        Args: None
        Ret:  None
    """

    logging.info( "---------------------------------------------------------------------------------------------")
    logging.info( "Checking for input files to copy..." )
    logging.info( "" )

    l_infiles = self.root.findall( './infiles/file' )
    
    if l_infiles == []:
      logging.info( "No input files to copy." )
    else:
      for file in l_infiles:
        logging.info( "Copying file: %s" % file.text )
        logging.info( "        to  : %s" % self.remoteDir.url )
        logging.info( "" )
        self.localDir.copy( file.text, self.remoteDir.url )
      logging.info( "Done copying input files." )

    logging.info( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" )
    logging.info( "" )


  def copyOutFiles( self ):
    """ Copies files from remote dir to local dir
        Args: None
        Ret:  None
    """

    logging.info( "---------------------------------------------------------------------------------------------")
    logging.info( "Checking for output files to copy..." )
    logging.info( "" )

    l_outfiles = self.root.findall( './outfiles/' )
    
    if l_outfiles == []:
      logging.info( "No output files to copy." )
    else:
      for file in l_outfiles:
        logging.info( "Copying file: %s" % file.text )
        logging.info( "        to  : %s" % self.localOutputDir.url )
        logging.info( "" )
        self.remoteDir.copy( file.text, self.localOutputDir.url )
      logging.info( "Done copying output files." )

    logging.info( "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" )
    logging.info( "" )


  def copyLogs( self, i_job ):
    """ Copies SAGA log files containing stdout and std error from remote dir to local log dir
        Args: None
        Ret:  None
    """

    l_logs = self.root.find( 'work/%s/logfiles' % i_job.type )

    if l_logs.find( 'stdout' ).text == "yes":
      self.remoteDir.copy( "%s" % i_job.desc.output, self.localLogDir.url )

    if l_logs.find( 'stderr' ).text == "yes":
      self.remoteDir.copy( "%s" % i_job.desc.error,  self.localLogDir.url )


  def parseGocdAgent( self ):
    """ Parses the keyword 'gocd_agent' from the xml file
        Args: None
        Ret : The name of the goCD agent running the script, or "NO_AGENT" if there is none
    """

    l_gocd_agent = self.root.find( 'gocd_agent' )

    if l_gocd_agent is not None:
      return l_gocd_agent.text
    else:
      return "NO_AGENT"


  def parseGocdPipeline( self ):
    """ Parses the keyword 'gocd_pipeline' from the xml file
        Args: None
        Ret : The name of the goCD pipeline containing the script, or "NO_PIPELINE" if there is none
    """

    l_gocd_pipeline = self.root.find( 'gocd_pipeline' )

    if l_gocd_pipeline is not None:
      return l_gocd_pipeline.text
    else:
      return "NO_PIPELINE"

  def parseUrl( self ):
    """ Parses the keyword 'remote_url' from the xml file
        Args: None
        Ret:  The url of the remote dir to connect to
    """

    l_url   = self.root.find( 'remote_url' )

    if l_url is not None:
      return l_url.text
    else:
      return ''


  def parseRemoteBasePath( self ):
    """ Parses the keyword 'remote_dir' from the xml file
        Args: None
        Ret:  Absolute path in the remote dir to copy files to
    """

    l_rem_bpath = self.root.find( 'remote_base_dir' )

    if l_rem_bpath is not None:
      return l_rem_bpath.text
    else:
      return ''


  def submit( self, i_job ):
    """ Submits job and monitors until completion or timeout, then copies SAGA logs
        Args: i_job - Job objoect representing job to be submitted
        Ret:  None
    """

    l_shellJobList = ['prejob', 'postjob', 'cleanup' ]

    if i_job.type is None:
      logging.info( "No work to be done." )
      logging.info( "" )
      return
    elif i_job.type in l_shellJobList:
      self.shellRun( i_job )
    elif i_job.type == 'job':
      self.slurmRun( i_job )
    else:
      logging.error( 'Error submitting job of type %s' % (i_job.type) )

    self.copyLogs( i_job )


  def shellRun( self, i_shellJob ):
    """ Runs shell job
        Args: i_shellJob  - ShellJob object representing job to be run
        Ret:  None
    """

    i_shellJob.desc.working_directory     = self.remoteDir.url.get_path()
    i_shellJob.sagajob                    = self.shellService.create_job( i_shellJob.desc )

    logging.info( "---------------------------------------------------------------------------------------------")
    logging.info( "Created shell job: %s" % (i_shellJob.desc.name) )
    logging.info( "ID               : %s" % (i_shellJob.sagajob.id)    )
    logging.info( "State            : %s" % (i_shellJob.sagajob.state) )
    logging.info( "---------------------------------------------------------------------------------------------")
    logging.info( "Starting shell job: %s..." % i_shellJob.desc.name )

    i_shellJob.sagajob.run()

    logging.info( "Started  shell job: %s."   % i_shellJob.desc.name )

    self.monitorJob( i_shellJob )


  def slurmRun( self, i_slurmJob ):
    """ Runs slurm job
        Args: i_slurmJob  - SlurmJob object representing job to be run
        Ret:  None
    """

    i_slurmJob.desc.working_directory      = self.remoteDir.url.get_path()
    i_slurmJob.sagajob                     = self.slurmService.create_job( i_slurmJob.desc )

    logging.info( "---------------------------------------------------------------------------------------------")
    logging.info( "Created slurm job: %s" % (i_slurmJob.desc.name) )
    logging.info( "ID               : %s" % (i_slurmJob.sagajob.id)    )
    logging.info( "State            : %s" % (i_slurmJob.sagajob.state) )
    logging.info( "---------------------------------------------------------------------------------------------")
    logging.info( "Starting slurm job: %s..." % i_slurmJob.desc.name )
    i_slurmJob.sagajob.run()
    logging.info( "Started  slurm job: %s."   % i_slurmJob.desc.name )

    self.monitorJob( i_slurmJob )


  def monitorJob( self, i_job ):
    """ Monitors and logs status of a job until termination or timeout
        Args: i_job - Job object representing job to be monitored
        Ret: None
    """

    l_endStates       = [ saga.job.CANCELED, saga.job.DONE, saga.job.FAILED, saga.job.SUSPENDED ]

    l_waitPeriod      = 30                    # Time to wait in between status checks (in sec)
    l_timeOutLimit    = 60*60                 # Time to wait in total before giving up (in sec)
    l_totalWaitIters  = math.trunc( l_timeOutLimit / l_waitPeriod )

    logging.info( "" )
    logging.info( "...waiting for %s to finish..." % i_job.desc.name )

    l_totalTime = 0
    while l_totalTime < l_timeOutLimit:
      logging.info( '%s ID        : %s' % (i_job.desc.name, i_job.sagajob.id) )
      logging.info( '%s State     : %s' % (i_job.desc.name, i_job.sagajob.state) )

      i_job.sagajob.wait(l_waitPeriod)

      if i_job.sagajob.state in l_endStates:
        break
      else:
        l_totalTime += l_waitPeriod

    if l_totalTime >= l_timeOutLimit:
      logging.error(  "Timeout waiting for %s to complete. Waited over %ss, checking every %ss." % (i_job.desc.name, l_timeOutLimit, l_waitPeriod) )
      logging.error(  "Workflow will proceed as if job is finished. Future dependencies may break.")
      logging.error(  "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
      logging.error(  "" )
    else: 
      logging.info(   "%s ID        : %s" % (i_job.desc.name, i_job.sagajob.id) )
      logging.info(   "%s State     : %s" % (i_job.desc.name, i_job.sagajob.state) )
      logging.info(   "%s Exitcode  : %s" % (i_job.desc.name, i_job.sagajob.exit_code) )
      logging.info(   "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" )
      logging.info(   "" )