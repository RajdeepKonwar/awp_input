##
# @file This file is part of AWP-ODC-OS.
#
# @author David Lenz (dlenz AT ucsd.edu)
# @author Rajdeep Konwar (rkonwar AT ucsd.edu)
#
# @section LICENSE
# Copyright (c) 2017, Regents of the University of California
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @section DESCRIPTION
# Job module within the flowTools package. Acts as a wrapper around the saga-python job class. Interfaces with xml config file.
##

import saga
import logging


class Job:
  """ Base class for all jobs """

  def __init__( self, i_type, i_root ):
    """ Constructor
        Args: i_type  - Type of job being created (prejob, job, postjob, cleanup)
              i_root  - Root of element tree used to parse the xml file
        Ret:  None
    """

    self.root       = i_root
    self.job_parent = self.root.find( './work/%s' % i_type )

    # Check that there is some work defined for this job
    if self.job_parent.find( './executable' ).text is None:
      self.type = None
      return
    else:
      self.type = i_type


    self.sagajob          = None
    self.desc             = saga.job.Description()
    self.initDesc()
    self.parseExecutable()
    self.parseArguments()

    l_slurm_job_types = [ 'job' ]

    if self.type in l_slurm_job_types:
      self.parseSlurmOptions()


  def initDesc( self ):
    """ Initializes basic fields in SAGA job description
        Args: i_type  - Type of job being constructed
              i_root  - Root of element tree used to parse the xml file
              io_desc - SAGA job descriptions
        Ret:  None
    """ 

    l_jobname                   = self.root.find( 'name' ).text

    self.desc.name                = '%s_%s' % ( l_jobname, self.type )
    self.desc.environment         = {}
    self.desc.output              = '%s.out' % self.desc.name
    self.desc.error               = '%s.err' % self.desc.name


  def parseExecutable( self ):
    """ Parses the keywork "executable" from the xml file
        Args: None
        Ret:  String containing executable
    """

    self.desc.executable = self.job_parent.find( 'executable' ).text


  def parseArguments( self ):
    """ Parses the keyword "arguments" from the xml file, getting all elements labeled "arg"
        and constructing a list of pairs "[key] [value]", where key is specified by the 
        attribute 'option' in the arg-element, and value is the text of the arg-element.

        Args: None
        Ret:  None
    """

    self.desc.arguments = []

    l_arg_elts = self.job_parent.findall( 'arguments/arg' )

    if l_arg_elts != []:
      for l_arg in l_arg_elts:

        # If the arg element has an 'option' attribute, create a key-value pair, with the 
        # value of the option attribute as the key, and the text of the element as the value
        if l_arg.text is not None:
          l_value = l_arg.text
        else:
          l_value = ''

        if 'option' in l_arg.keys():
          l_key   = l_arg.get( 'option' )
          self.desc.arguments.append( "%s %s" % ( l_key, l_value ) )
        else:
          self.desc.arguments.append( "%s" % l_value )


  def parseSlurmOptions( self ):
    """ Parses the keyword 'slurm_options' from the xml file and sets saga description accordingly
        Args: None
        Ret:  None
    """

    l_options = self.job_parent.find( 'slurm_options' )

    if l_options is not None:
      self.desc.project             = l_options.find( 'project').text            # -A Allocation Number
      self.desc.queue               = l_options.find( 'partition' ).text         # -p Queue/Partition Name
      self.desc.number_of_processes = l_options.find( 'num_process' ).text       # -n Total Number of MPI Tasks
      self.desc.processes_per_host  = l_options.find( 'process_per_host' ).text  # --tasks-per-node
      self.desc.wall_time_limit     = l_options.find( 'wall_time_limit' ).text   # -t Wall Time for the Job (min)
    else:
      logging.warning( "No options found when attempting to parse slurm settings" )
      logging.warning( "" )
