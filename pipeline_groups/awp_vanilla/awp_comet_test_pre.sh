#!/bin/bash
module li
module unload intel/2013_sp1.2.144
module load gnu/4.9.2
module load mvapich2_ib/2.1
module li

mkdir -p input
mkdir -p run
mkdir -p run/logs
mkdir -p run/raw
tar -zxf gocd_input.tar.gz -C input
tar -zxf awp-odc-os.tar.gz
cd awp-odc-os
CXX=mpicxx CC=mpicc scons kernel=vanilla parallel=mpi+omp mode=debug