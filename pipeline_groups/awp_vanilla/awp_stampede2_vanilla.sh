#!/bin/bash



mkdir -p input
mkdir -p run
mkdir -p run/logs
mkdir -p run/raw
tar -zxf gocd_input.tar.gz -C input
tar -zxf awp-odc-os.tar.gz
cd awp-odc-os
CXX=mpicxx CC=mpicc scons kernel=vanilla parallel=mpi+omp mode=debug