#!/bin/bash

parent="${PWD}"
grandparent="${parent%/*}"
g_grandparent="${grandparent%/*}"

# Double check to make sure we're in a directory that looks like 
# ~/AGENT/pipelines/PIPELINE for the current Agent and Pipeline 
# running this script
if[ "${parent##/*/}" == "TMPL_AGENT" ] then
	if[ "${grandparent##/*/}" == "pipelines" ] then
		if[ "${g_grandparent##/*/}" == "TMPL_PIPELINE" ]; then
			rm -rvf source
			rm -rv ./*
		fi
	fi
fi