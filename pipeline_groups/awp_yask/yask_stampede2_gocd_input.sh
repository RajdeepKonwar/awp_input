#!/bin/bash

mkdir -p input
mkdir -p run
mkdir -p run/logs
mkdir -p run/raw

tar -zxvf gocd_input.tar.gz -C input
cd ../

tar -zxvf awp-odc-os.tar.gz
cd awp-odc-os

CXX=mpiicpc CC=mpiicc scons kernel=yask parallel=mpi+omp cpu_arch=knl mode=debug