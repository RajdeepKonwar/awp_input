##
# @file This file is part of AWP-ODC-OS.
#
# @author David Lenz (dlenz AT ucsd.edu)
# @author Rajdeep Konwar (rkonwar AT ucsd.edu)
#
# @section LICENSE
# Copyright (c) 2017, Regents of the University of California
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# @section DESCRIPTION
# Generates, submits and runs a slurm script using
# saga-python (https://github.com/radical-cybertools/saga-python) for
# benchmarking AWP-ODC-OS code via GoCD (https://www.gocd.org/).
##

import os
import sys
import saga
import argparse
import logging
import xml.etree.ElementTree as ET

from flowTools import job
from flowTools import connection as cnx

"""
Workflow:
    1.  COPY IN FILES 
          - Copy all necessary files from local resource to remote resource (this includes all scripts/executables)

    2.  RUN PREJOB  [REMOTE]
          - Run a shell script on login node of remote resource to prepare for main job 
          - Typically low intensity tasks like compilation, configuration, creation of necessary files/directories

    3.  RUN MAIN JOB [REMOTE]
          - Submit a slurm script to run on compute nodes of remote resource

    4.  RUN POSTJOB [REMOTE]
          - Run a shell script on login node of remote resource to finalize any output from main job
          - Could be things like generating summary reports or gathering all output files to a single location

    5.  COPY OUT FILES
          - Copy all specified files from remote resource to local resource

    6.  CLEANUP [REMOTE]
          - Run a shell script on login node of remote resource to clean up workspace and prepare for future jobs
"""

def initParser():
  """ Creates the argument parser to read inputs from command line
        Args: NONE
        Ret : Argument Parser with arguments properly configured
  """

  o_parser = argparse.ArgumentParser( description = "Configures and submits jobs to run on remote resources.", 
                                      usage       = "%(prog)s -c CERT -x XML1 [XML2 ...] [job options]",
                                      epilog      = "Note: If no job type options are specified, all job types will be run" )

  o_parser.add_argument( '-c', '--cert',required  = True,
                                        help      = "path to the X509 certificate" )

  o_parser.add_argument( '-x', '--xml', nargs     = '+' ,
                                        required  = True, 
                                        help      = "path(s) to the xml configuration file(s)" )

  o_parser.add_argument( '--pre',       required  = False         ,
                                        action    = 'append_const' ,
                                        const     = 'pre'         ,
                                        dest      = 'jobs'        ,
                                        help      = "run the prejob portion of the work" )

  o_parser.add_argument( '--main',      required  = False         ,
                                        action    = 'append_const' ,
                                        const     = 'main'        ,
                                        dest      = 'jobs'        ,
                                        help      = "run the main (slurm) portion of the work" )

  o_parser.add_argument( '--post',      required  = False         ,
                                        action    = 'append_const' ,
                                        const     = 'post'        ,
                                        dest      = 'jobs'        ,
                                        help      = "run the postjob portion of the work" )

  o_parser.add_argument( '--clean',     required  = False         ,
                                        action    = 'append_const' ,
                                        const     = 'clean'       ,
                                        dest      = 'jobs'        ,
                                        help      = "run the cleanup portion of the work" )

  o_parser.add_argument( '--infiles',   required  = False         ,
                                        action    = 'append_const' ,
                                        const     = 'infiles'     ,
                                        dest      = 'jobs'        ,
                                        help      = "copy in files from local resource to remote resouce" )

  o_parser.add_argument( '--outfiles',  required  = False         ,
                                        action    = 'append_const' ,
                                        const     = 'outfiles'    ,
                                        dest      = 'jobs'        ,
                                        help      = "copy out files from remote resource to local resouce" )

  o_parser.add_argument( '--all',       required  = False         ,
                                        action    = 'append_const' ,
                                        const     = 'all'         ,
                                        dest      = 'jobs'        ,
                                        help      = "run all job types (supercedes all other job type options when specified)" )

  return o_parser




def main():
  """Main Function"""

  # Set up the logger
  logging.basicConfig(  level  = logging.INFO,
                        format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s' )

  # Parse command line arguments
  l_parser      = initParser()
  l_args        = l_parser.parse_args()

  l_certPath    = l_args.cert
  l_xmlPathList = l_args.xml

  if l_args.jobs is not None:
    l_joblist = l_args.jobs
  else:
    l_joblist = [ 'all' ]


  # Iterate over all xml files specified
  for l_xmlPath in l_xmlPathList:
    
    # Parse the XML file and get root
    l_tree        = ET.parse( l_xmlPath )
    l_root        = l_tree.getroot()

    # Create connection
    l_connection  = cnx.Connection( l_root, l_certPath, l_xmlPath )

    # Begin Workflow
    if 'infiles' in l_joblist or 'all' in l_joblist:
      l_connection.copyInFiles()

    if 'pre' in l_joblist or 'all' in l_joblist:
      l_prejob  = job.Job( 'prejob', l_root )
      logging.info( "Submitting Pre Job..." )
      l_connection.submit( l_prejob )

    if 'main' in l_joblist or 'all' in l_joblist:
      l_mainjob = job.Job( 'job', l_root)
      logging.info( "Submitting Main Job..." )
      l_connection.submit( l_mainjob )

    if 'post' in l_joblist or 'all' in l_joblist:
      l_postjob = job.Job( 'postjob', l_root )
      logging.info( "Submitting Post Job..." )
      l_connection.submit( l_postjob )

    if 'outfiles' in l_joblist or 'all' in l_joblist:
      l_connection.copyOutFiles()

    if 'clean' in l_joblist or 'all' in l_joblist:
      l_cleanup = job.Job( 'cleanup', l_root )
      logging.info( "Submitting Cleanup Job..." )
      l_connection.submit( l_cleanup )
    # End Workflow

    logging.info( "All jobs finished." )


if __name__ == "__main__":
    sys.exit( main() )